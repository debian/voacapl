Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/jawatson/voacapl
Upstream-Name: voacapl
Upstream-Contact: James Watson <jimwatson@mac.com>

Files: *
Copyright: 2007-2020 James Watson <jimwatson@mac.com>
License: special and CC0-1.0

Files: debian/*
Copyright: 2024-2025 David da Silva Polverari <polverari@debian.org>
License: GPL-3+

Files: voacapl/itshfbc/bin/dst/dst2ascii.f90
Copyright: 2018 James Watson <jimwatson@mac.com>
License: GPL-3+

Files: voacapl/itshfbc/bin/dst/dst2csv.f90
Copyright: 2018 James Watson <jimwatson@mac.com>
License: GPL-3+

Files: voacapl/itshfbc/bin/dst/f90getopt.f90
Copyright: 2014 Hani Andreas Ibrahim <hani.ibrahim@gmx.de>
License: GPL-3+
Comment:
 Authorship was referred in both the dst2ascii.f90 and dst2csv.f90 files found
 in the voacapl/itshfbc/bin/dst/ directory. Licensing information was found in
 the referred upstream repository.

License: special
 Disclaimer:
 .
 The software contained within was developed by an agency of the
 U.S. Government. NTIA/ITS has no objection to the use of this
 software for any purpose since it is not subject to copyright
 protection in the U.S.
 .
 No warranty, expressed or implied, is made by NTIA/ITS or the
 U.S. Government as to the accuracy, suitability and functioning
 of the program and related material, nor shall the fact of
 distribution constitute any endorsement by the U.S. Government.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in "/usr/share/common-licenses/CC0-1.0".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
